define :demo, :action => :create, :owner => "root", :group => "root", :mode => 0755, :packages => {} do
	if params[:action]==:create 
	 	directory '/tmp/chefdirectory' do
         	owner 'root'
         	group 'root'
         	mode '0755'
         	action :create
		end
	end
	if params[:action]==:delete
		directory '/tmp/chefdirectory' do
		action :delete
		#recursive :true	
		end
	end
end

